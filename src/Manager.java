package threesolid;
// no changes made other than moving to new file
// single repsonsibility principle in use for readability
// no other principles needed
/**  **/
class Manager {
    IWorker worker;

    public void Manager() {

    }
    public void setWorker(IWorker w) {
        worker=w;
    }

    public void manage() {
        worker.work();
    }
}
