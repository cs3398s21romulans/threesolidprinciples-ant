package threesolid;
// super worker: assumption that implied code adds child function for working harder,
// but as a mere mortal, still must eat
// set functions to private, keeping functions from being modified (open/close)
//using only basics for Superworker, allow for children to add more function (substitute principle)
// no other principles needed
/**  **/

public class SuperWorker implements IWorker, ILaunch{
    private void work() {
        //.... working much more
    }

    private void eat() {
        //.... eating in launch break
    }
}