package threesolid;
// created from original IWorker interface. since we added a robot class,
// eating is only required for Mortal Beings
// use of interface segregation principle to only contain used functions.
// no other principle applies
/** Alexander **/
public interface ILaunch {
    public void eat();
}
