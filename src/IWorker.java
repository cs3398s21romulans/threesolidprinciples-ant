package threesolid;
// remainder of IWorker. since all employees are required to work,
// they will all use this function.
// use of interface segregation principle to only contain used functions.
// no other principle applies
/** Alexander **/
public interface IWorker {
    public void work();
}
