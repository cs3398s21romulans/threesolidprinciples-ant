package threesolid;
// class moved to own file. since worker needs to both eat and all employees must work
// set functions to private, keeping functions from being modified (open/close)
// no other principples needed
/**  **/
class Worker implements IWorker, ILaunch{
	private void work() {
		// ....working
	}

	private void eat() {
		//.... eating in launch break
	}
}
